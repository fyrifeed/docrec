import requests
import streamlit as st
import json
import os

st.title("Оцифровка документов")

pdf = st.file_uploader("Выберите файл")

if st.button("Запустить распознавание"):
    if pdf is not None:
        url = 'http://back:8080/excel'
        files = {'file': pdf}
        result = requests.post(url=url, files=files)
        jsoned = json.loads(result.content.decode('utf-8'))
        fname = os.path.split(jsoned['filepath'])[-1]
        with open(jsoned['filepath'], 'rb') as f:
            st.download_button('Скачать excel файл', f, file_name= fname)
