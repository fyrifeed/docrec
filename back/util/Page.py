import cv2
import math
import numpy as np
from scipy import ndimage
import fitz
import logging

from . import TransformationInterface as tr
class Page:

    def __init__(self, image, number) -> None:
        self.number = number
        self.image = image
        self.is_blue_pen = Page.is_blue_pen(image)
    
    @staticmethod
    def is_blue_pen(image):
        return np.mean(tr.TransformationInterface.blue_only(image)[38 : 38 + 936, 205 : 205 + 1086]) < 254.7
    
    
    def get_page_with_corrected_skew(self, resize = False):
        return tr.TransformationInterface.correct_skew(self.image, resize = resize)
    
    
        
    
    
    
    
    
        