from . import Page
import fitz
import numpy as np
from . import TransformationInterface as tr
class Doc:
    def __init__(self, filepath) -> None:
        """
        Opening pdf file and making the list of pages in ndarray format

        Args:
            filepath (str): path to a pdf file with tables
        """
        pages = []
        doc = fitz.open(filepath)
        
        for n in range(doc.page_count):
            page = doc.load_page(n)
            pix = page.get_pixmap() 
            image = np.frombuffer(pix.samples, 
                dtype=np.uint8).reshape(pix.h, pix.w, pix.n)
            image = np.ascontiguousarray(image[..., [2, 1, 0]])
            pages.append(Page.Page(tr.TransformationInterface.correct_skew(image, resize=True), n))
        
        self.pages = pages
        self.rows_names = [
            'Номер объекта',
            'Таб. номер 1',
            'Таб. номер 2',
            'Таб. номер 3',
            'Таб. номер 4',
            'Таб. номер 5',
            'Грунтование стен и потолков',									
            'Обработка поверхности противогрибковым составом',
            'Штукатурка стен и потолков',		
            'Шпатлевание стен и потолков',
            'Оклейка стен обоями',				
            'Окраска стен и потолков за 2 раза',							
            'Облицовка стен керамической плиткой',							
            'У-во коробов из ГКЛВ',
            'Укладка керамогранита на пол',									
            'Укладка ламината',									
            'Установка плинтуса',									
            'Установка порогов',									
            'Заливка пола ровнителем',								
            'Устройство цементно-песчаной стяжки',
            'Установка м/к дверей',									
            'Установка доборов',									
            'Установка наличников',
            'Штукатурка откосов',									
            'Шпатлевание откосов',								
            'Устройство откосов из ГКЛВ',									
            'Окраска откосов за 2 раза',									
            'Установка ПВХ уголка/герметизация примыканий (акрил)',
            'Грунтование стен и потолков',								
            'Обработка поверхности противогрибковым составом',									
            'Штукатурка стен и потолков',								
            'Шпатлевание стен и потолков',									
            'Окраска стен и потолков за 2 раза',
            'Устройство цементно-песчаной стяжки', 									
            'Устройство плинтуса из керамогранита',									
            'Укладка керамогранита на пол',
            'Утепление стен/откосов/подоконников',
            '',
            ''
                ]
    
    
        
    
        
    
    
        