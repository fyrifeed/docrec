import cv2, os
import numpy as np
from copy import deepcopy
import json

from . import CutterInterface as ct
def sort2(val):   #helper for sorting by y
    return val[1] 


def group_coords_by_rows(coords):
    res = []
    row = []
    prev_y = 0
    coords.sort() #sort by x
    coords.sort(key = sort2) # sort by y
    for coord in coords:
        x,y,w,h = coord
        if y > prev_y + 10: #new row if y is changed
            res.append(row)
            row = []
        row.append(coord)
        prev_y = y
    res.append(row)
    return res



def fill_missing(grouped_coords):
    gc = deepcopy(grouped_coords)
    to_append = []
    for i in gc:
        for j in range(1, len(i)):
            x1, y1, w1, h1 = i[j]
            x2, y2, w2, h2 = i[j - 1]
            if abs(x1 - x2) > 40:
                to_append.append((x2 + w2 + 1, y2, w2, h2))
        for k in to_append:
            i.append(k)
        to_append.clear()
        for i in gc:
            i.sort(key=lambda x: x[0])
    return gc


class TableRecognitionInterface:
    """"""
    @staticmethod
    def get_recognized_tables(image, area_treshold_lower = 4000, area_treshold_upper = np.inf, save = False, counter = 0):
        """
        This func precisely works with table recognition.
        After a couple of transformations it finds coordinates of tables.
        In this exact example there is 3 tables.

        Args:
            image (ndarray): given image
            area_treshold_lower (int, optional): lower bound of treshold. Defaults to 4000.
            area_treshold_upper (int, optional): upper bound of treshold. Defaults to np.inf.
            save (bool, optional): If it is needed to save image for bugtracking. Defaults to False.
            counter (int) : number of saved image

        Returns:
            list[(x,y,w,h)]: list of tuples of coordinates
        """
        if len(image.shape) == 3:
            gray= image[:,:,0]
        else:
            gray = image.copy()
        coords = []
        # adaptive threshold
        thresh = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV,51,9)

        # Fill rectangular contours
        cnts = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
        for c in cnts:
            cv2.drawContours(thresh, [c], -1, (255,255,255), -1)

        # Morph open
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (9,9))
        opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=4)

        # Draw rectangles, the 'area_treshold' value was determined empirically
        cnts = cv2.findContours(opening, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
        for c in cnts:
            if area_treshold_upper > cv2.contourArea(c) > area_treshold_lower :
                x,y,w,h = cv2.boundingRect(c)
                coords.append((x, y, w, h))
                if save:
                    cv2.rectangle(image, (x, y), (x + w, y + h), (36,255,12), 3)
        if save:
            cv2.imwrite(os.path.join(r'./big_table_check' , f'image{counter}.jpg'), image)
            cv2.waitKey(0)
        return coords
    
    
    @staticmethod
    def get_subtables(image, save = False, counter = 0):
        """
        It is used to incapsulate recognition and cropping of tables

        Args:
            image (ndarray): given image
            save (bool, optional): If it is needed to save image for bugtracking. Defaults to False.
            counter (int) : number of saved image
        Returns:
            list[ndarray]: in this case 3 tables as an images
        """
        coords = TableRecognitionInterface.get_recognized_tables(image.copy(), save = save, counter=counter)
        num_of_tables = len(coords)
        if num_of_tables != 3:
            with open("tables.json", "r") as f:
                coords = json.load(f)
        return ct.CutterInterface.get_cropped_images(image.copy(), coords)
    
    
    @staticmethod
    def get_subtables2(image, img, save = False, counter = 0):
        """
        It is used to incapsulate recognition and cropping of tables if schema for cropping is given

        Args:
            image (ndarray): given schema
            img (ndarray): given image
            save (bool, optional): If it is needed to save image for bugtracking. Defaults to False.
            counter (int) : number of saved image
        Returns:
            list[ndarray]: in this case 3 tables as tuples of two elements: image and schema
        """
        coords = TableRecognitionInterface.get_recognized_tables(image.copy(), save = save, counter=counter)
        num_of_tables = len(coords)
        if num_of_tables != 3:
            with open("tables.json", "r") as f:
                coords = json.load(f)
        return ct.CutterInterface.get_cropped_images_with_schema(image.copy(), img, coords)
    
    
    @staticmethod
    def get_cells_coords(img, h_lower = 22, w_lower = 22, area_upper = 3000, save = False, fill = True):
        """
        Recognize cells coords according to given constraints
        Returns as formatted by rows list of lists

        Args:
            img (ndarray): given image
            h_lower (int, optional): h lower bound . Defaults to 22.
            w_lower (int, optional): w lower bound. Defaults to 22.
            area_upper (int, optional): area upper bound. Defaults to 3000.

        Returns:
            list[list[coords]]: first index - number of a row in the table, second index - number of a column
        """
        if len(img.shape) == 3:
            gray_image= img[:,:,0]
        else:
            gray_image = img.copy()
        
        if save:
            output = deepcopy(gray_image)

        # (1) thresholding image
        thresh_value = cv2.threshold(gray_image, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

        # (2) dilating image to glue letter with e/a
        kernel = np.ones((30, 30),np.uint8)    
        dilated_value = cv2.dilate(thresh_value,kernel,iterations = 3)

        # (3) looking for countours
        contours, hierarchy = cv2.findContours(thresh_value ,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

        # (4) extracting coordinates and filtering them empirically 
        coordinates = []
        for contour in contours:
            x,y,w,h = cv2.boundingRect(contour)
            if h> h_lower and w > w_lower and h*w < area_upper:
                h_gap = 0
                w_gap = 0
                # if save:
                #     cv2.rectangle(output, (x, y), (x + w + w_gap, y + h + h_gap), (36,255,12), 3)
                coordinates.append((x, y, w + w_gap ,h + h_gap))
        if fill:
            res = fill_missing(group_coords_by_rows(coordinates))
        else:
            res = group_coords_by_rows(coordinates)
        if save:
            cv2.imwrite(os.path.join('./sheets_pdf/detected_cells/' , f'img.jpg'), output)
            cv2.waitKey(0)
        return res