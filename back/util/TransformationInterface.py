import cv2
from scipy import ndimage
import numpy as np
import torch

class TransformationInterface:
    
    """Picture transformations interface"""
    
    @staticmethod
    def correct_skew(image, resize = False, delta=1, limit=5):
        """
        Makes image straight vertical according to the lines on it.

        Args:
            image (ndarray): image
            resize (bool, optional): It is used to standartize pages' size. Defaults to False.
            delta (int, optional): Defaults to 1.
            limit (int, optional): Defaults to 5.

        Returns:
            ndarray : correctly skewed image 
        """
        if resize:
            image = cv2.resize(image, None, fx=1500 / image.shape[0], fy=1000 / image.shape[1], interpolation= cv2.INTER_CUBIC)
        def determine_score(arr, angle):
            data = ndimage.interpolation.rotate(arr, angle, reshape=False, order=0)
            histogram = np.sum(data, axis=1, dtype=float)
            score = np.sum((histogram[1:] - histogram[:-1]) ** 2, dtype=float)
            return histogram, score
        if len(image.shape) == 3:
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        else:
            gray = image.copy()
        thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1] 

        scores = []
        angles = np.arange(-limit, limit + delta, delta)
        for angle in angles:
            histogram, score = determine_score(thresh, angle)
            scores.append(score)

        best_angle = angles[scores.index(max(scores))]

        (h, w) = image.shape[:2]
        center = (w // 2, h // 2)
        M = cv2.getRotationMatrix2D(center, best_angle, 1.0)
        corrected = cv2.warpAffine(image, M, (w, h), flags=cv2.INTER_CUBIC, \
                borderMode=cv2.BORDER_REPLICATE)
        return corrected
    
    
    @staticmethod
    def blue_only(img):
        """
        It vanishes all the non-blue objects from the pic.
        All the blue symbols become gray and contoured.

        Args:
            img (ndarray): image

        Returns:
            image : ndarray 
        """
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        lower_blue = np.array([100,50,50])
        upper_blue = np.array([130, 255, 255])
        mask = cv2.inRange(hsv, lower_blue, upper_blue)
        res = 255 - cv2.bitwise_and(img, img, mask= mask)
        gray_img = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)
        thresh = cv2.threshold(gray_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        thresh = cv2.GaussianBlur(thresh, (5,5), 0)
        return thresh
    
    
    @staticmethod
    def remove_lines(image):
        """
        Remove vertical and horizontal lines from picture.

        Args:
            image (ndarray): given image

        Returns:
            ndarray : image with reduced vertical and horizontal lines
        """
        if len(image.shape) == 3:
            gray_image= image[:,:,0]
        else:
            gray_image = image.copy()
        thresh = cv2.threshold(gray_image, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
        
        vertical_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1,30))
        detected_lines = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, vertical_kernel, iterations=2)
        cnts = cv2.findContours(detected_lines, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
        for c in cnts:
            cv2.drawContours(image, [c], -1, (255,255,255), 2)
            
        horizontal_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (30,1))
        detected_lines = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, horizontal_kernel, iterations=2)
        cnts = cv2.findContours(detected_lines, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
        for c in cnts:
            cv2.drawContours(image, [c], -1, (255,255,255), 2)

        # Repair image
        repair_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5,5))
        result = 255 - cv2.morphologyEx(255 - image, cv2.MORPH_CLOSE, repair_kernel, iterations=1)
        return result
    
    @staticmethod
    def gain_division(img):
        """       
        Performs gain division transformation of an image. Used to make symbols readable after reducing vertical and horizontal lines.

        Args:
            img (ndarray): given image

        Returns:
            ndarray : transformed image
        """
        filterSize = 5
        imageMedian = cv2.medianBlur(img, filterSize)

        kernelSize = 15
        maxKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernelSize, kernelSize))
        localMax = cv2.morphologyEx(imageMedian, cv2.MORPH_CLOSE, maxKernel, None, None, 1, cv2.BORDER_REFLECT101)

        gainDivision = np.where(localMax == 0, 0, (img/localMax))

        gainDivision = np.clip((255 * gainDivision), 0, 255)

        gainDivision = gainDivision.astype("uint8")
        if len(img.shape) == 3:
            grayscaleImage= img[:,:,0]
        else:
            grayscaleImage = img.copy()
        
        grayscaleImage = np.uint8(cv2.normalize(grayscaleImage, grayscaleImage, 0, 255, cv2.NORM_MINMAX))
        threshValue, binaryImage = cv2.threshold(grayscaleImage, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)

        threshValue = threshValue
        _, binaryImage = cv2.threshold(grayscaleImage, threshValue, 255, cv2.THRESH_BINARY)
        binaryImage = 255 - binaryImage

        componentsNumber, labeledImage, componentStats, componentCentroids = \
        cv2.connectedComponentsWithStats(binaryImage, connectivity=4)

        minArea = 10

        remainingComponentLabels = [i for i in range(1, componentsNumber) if componentStats[i][4] >= minArea]

        filteredImage = np.where(np.isin(labeledImage, remainingComponentLabels) == True, 0, 255).astype("uint8")
        return filteredImage
    
    @staticmethod
    def transform_pipeline(image):
        """
        This func is used for image preprocessing before recognition

        Args:
            image (ndarray): given image

        Returns:
            ndarray : image ready for recognition
        """
        if len(image.shape) == 3:
            gray_image= cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        else:
            gray_image = image.copy()
        h, w = gray_image.shape
        if (h, w) != (28, 28):
            thresh = cv2.resize(gray_image, (0, 0), fx=28 / h, fy=28 / w, interpolation= cv2.INTER_CUBIC)
        #
        # sharpen_kernel = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])
        # thresh = cv2.filter2D(thresh, -1, sharpen_kernel)
        # thresh = cv2.threshold(thresh, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        thresh = torch.from_numpy(thresh).type(torch.float) / 255
        return np.asarray(thresh)