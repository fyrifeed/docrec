class CellsNumberException(Exception):
    def __init__(self, name_of_func, min_len, max_len, number_of_rows):
        self.name_of_func = name_of_func
        self.number_of_rows = number_of_rows
        self.max_len = max_len
        self.min_len = min_len


    def __str__(self):
        return f'Invalid number of cells detected in function {self.name_of_func}:\nNumber of rows is {self.number_of_rows}\nMin len of row is {self.min_len}\nMax len of row is {self.max_len}'

class NumberOfTablesException(Exception):
    def __init__(self, number_of_tables):
        self.number_of_tables = number_of_tables


    def __str__(self):
        return f'Invalid number of cells detected in function {self.name_of_func}:\nNumber of tables is {self.number_of_tables}'