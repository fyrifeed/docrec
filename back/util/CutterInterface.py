import numpy as np


class CutterInterface:
    """Different cases of image cutting interface"""
    
    @staticmethod
    def get_cropped_images(image, coordinates, ascending = True):
        """
        Used to cropp image to a detected parts(cells or tables)

        Args:
            image (ndarray): given image
            coordinates (list of tuples): list of coordinates in format (x,y,w,h)

        Returns:
            list[ndarray]: list of parts of the image
        """
        print(coordinates)
        if len(image.shape) == 3:
            image= image[:,:,0]
        else:
            image = image.copy()
        res = []
        if ascending:
            coordinates.sort(key = lambda x: -x[0]-x[1])
        else:
            coordinates.sort(key = lambda x: x[0]+x[1])
        for coord in coordinates:
            x,y,w,h = coord
            crop_img = image[y:y+h, x:x+w]
            res.append(crop_img)
        return res
    
    @staticmethod
    def get_cropped_images_with_schema(image, img, coordinates):
        """
        Used to cropp image to a detected parts of another image(cells or tables)

        Args:
            image (ndarray): given schema
            img (ndarray): given image
            coordinates (list of tuples): list of coordinates in format (x,y,w,h)

        Returns:
            list[ndarray]: list of parts of the first image
            list[ndarray]: list of parts of the second image
        """
        print(coordinates)
        if len(image.shape) == 3:
            image= image[:,:,0]
        else:
            image = image.copy()
        res_clear = []
        res_schema = []
        coordinates.sort(key = lambda x: -x[0]-x[1])
        for coord in coordinates:
            x,y,w,h = coord
            res_clear.append(img[y:y+h, x:x+w])
            res_schema.append(image[y:y+h, x:x+w])
        return res_clear, res_schema
    
    
    @staticmethod
    def concat_two_imgs(img1, img2):
        """
        Used to concatenate two images

        Args:
            img1 (ndarray): first image
            img2 (ndarray): second image

        Returns:
            ndarray : result of vertical concatenation of two images in format [img1, img2]
        """
        h1, w1 = img1.shape[:2]
        h2, w2 = img2.shape[:2]
        vis = np.zeros((max(h1, h2), w1+w2), np.uint8)
        vis[:h1, :w1] = img1
        vis[:h2, w1:w1+w2] = img2
        return vis
    