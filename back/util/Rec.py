import re
# import TransformationInterface as tr
import numpy as np
import cv2
import os
import pandas as pd
from pathlib import Path
from copy import deepcopy
import json
import easyocr
import torch
from . import TransformationInterface as tr
from . import CutterInterface as ct
from . import TableRecognitionInterface as TRI


class Recognizer:
    def __init__(self) -> None:
        # self.digitRec = PaddleOCR(use_angle_cls=True, lang='en')
        # self.rusRec = PaddleOCR(use_angle_cls=True, lang='ru')
        self.model = torch.jit.load('model_conv2_no_leaks.pt')
        self.model.eval()
        self.reader = easyocr.Reader(['ru']) 
        with open("coords.json", "r") as f:
            data = json.load(f)
            self.big_table_coords = data['big_table_coords']
            self.people_nums_coords = data['people_num_coords']
        
    @staticmethod
    def is_empty(img):
        np.reshape(img, (-1,1))
        if np.mean(img) < 250:
            return False
        else:
            return True    
    
    
    @staticmethod
    def accuracy_increaser(ch):
        ch = ch.lower()
        if (dec := re.sub(r'[^\d]',  '', ch)) != '':
            return dec
        else:
            if '&' in ch:
                return '8'
            dec = re.sub(r'[^\D]',  '', ch)
            if dec in 'h':
                return '4'
            elif dec in 'g':
                return '9'
            return ch

    def predict_label(self, img):
        w, h = img.shape
        if (w, h) != (28, 28):
            img = cv2.resize(img, (0, 0), fx=28 / h, fy=28 / w, interpolation=cv2.INTER_CUBIC)
        img = torch.from_numpy(img).type(torch.float).unsqueeze(0).unsqueeze(0)
        # print(img.shape)
        with torch.no_grad():
            logps = self.model(img)
            ps = torch.exp(logps)
            pred_probab = torch.nn.Softmax(dim=1)(logps)
            y_pred = pred_probab.argmax(1)
        return float(torch.max(pred_probab)), int(y_pred)
    def recognize_digit(self, img):
        digit_list = '0123456789'
        transformed = tr.TransformationInterface.transform_pipeline(img)
        if (flag := not Recognizer.is_empty(img[10:-10, 10:-10])):
            model_conf, digit = self.predict_label(transformed)
            if model_conf < 0.65:
                conf = '-'
            else:
                conf = '+'
            return str(digit), conf
        else:
            return '', ''

            # if recognized:
            #     digit = Recognizer.accuracy_increaser(recognized[0][1])
            #     if digit not in digit_list and flag:
            #         digit = '?'
            #
            #     confidence = recognized[0][2]
            #     if float(confidence) > 0.8:
            #         conf= '+'
            #     else:
            #         conf = '-'
            # else:
            #     if (len(eo := self.reader.readtext(img, detail = 0, allowlist='0123456789')) != 1):
            #         digit = '?'
            #         conf = '?'
            #     else:
            #         digit = eo[0]
            #         conf = '+'
                    



    def recognize_russian_char(self, img):
        transformed = tr.TransformationInterface.transform_pipeline(img)
        conf = ''
        letter = ''
        if (flag := not Recognizer.is_empty(transformed)):
            recognized = self.reader.readtext(transformed, detail =1)

            if recognized:
                letter = recognized[0][1]
                if flag:
                    letter = '?'
                    
                confidence = recognized[0][2]
                if float(confidence) > 0.8:
                    conf= '+'
                else:
                    conf = '-'
            else:
                if (len(eo := self.reader.readtext(img, detail = 0)) != 1):
                    digit = '?'
                    conf = '?'
                else:
                    digit = eo[0]
                    conf = '+'
        return letter, conf


    def recognize_line(self, img):
        transformed = tr.TransformationInterface.transform_pipeline(img)
        recognized = self.reader.readtext(transformed, detail =1)

        if recognized:
            line = recognized[0][1]
        else:
            rec2 = self.reader.readtext(img, detail = 0)
            if len(rec2) > 0:
                line = ''.join(rec2)
            else:
                line = '?'
        return line
        
        
    def recognize_object_number(self, object_table):
        
        # if len(object_table.shape) == 3:
        #     gray_image= cv2.cvtColor(object_table, cv2.COLOR_BGR2GRAY)
        # else:
        #     gray_image = object_table.copy()
        pic = object_table[:-105, 250:-50]
        recognized = self.recognize_line(pic)
        return recognized, pic
    
    
    def recognize_people_numbers(self, people_number_schema, people_number_clear):
        # cv2.imwrite(os.path.join(r'prototyping/sheets_pdf/' , f'people_number.jpg'), people_number_schema)
        # cv2.waitKey(0)
        cells_coords = TRI.TableRecognitionInterface.get_cells_coords(people_number_schema, 22, 22, fill=False, save = True)
        cells_coords =[sorted(i, key = lambda x: x[0], reverse=False) for i in cells_coords]

        cells_lens = [len(i) for i in cells_coords]
        num_of_rows = len(cells_lens)
        min_num_of_cells_in_row = min(cells_lens)
        max_num_of_cells_in_row = max(cells_lens)
        if num_of_rows != 5 or min_num_of_cells_in_row != 10 or max_num_of_cells_in_row != 10:
            cells_coords = self.people_nums_coords
            # raise CellsNumberException('recognize_people_numbers', num_of_rows, min_num_of_cells_in_row, max_num_of_cells_in_row)
        cropped_cells = [ct.CutterInterface.get_cropped_images(people_number_clear, i, ascending=False) for i in cells_coords]
        rows = []
        confidence = []
        pics = []
        
        is_empty_row = False
        for cells in cropped_cells:
            cur_row = ''
            cur_confidence = ''
            vis = ct.CutterInterface.concat_two_imgs(cells[0], cells[1])
            
            for i in cells[2:]:
                vis = ct.CutterInterface.concat_two_imgs(vis, i)
            pics.append(vis)
            for num, cell in enumerate(cells):
                if num == 0:
                    if (is_empty_row := Recognizer.is_empty(cell)):
                        pass
                if not is_empty_row:
                    if num < 2:
                        symbol, conf = self.recognize_russian_char(cell)
                    else:
                        symbol, conf = self.recognize_digit(cell)
                    cur_row += symbol
                    cur_confidence += conf

            rows.append(cur_row)
            confidence.append(cur_confidence)
        return rows, confidence, pics
    
    def black_pen_pipeline(self, image, doc_num, rows_names):
        tables = TRI.TableRecognitionInterface.get_subtables(image)
        res_info = deepcopy(tables[1][30:, 650:])
        
        
        object_num, object_pic = self.recognize_object_number(tables[2])
        
        people_nums_vals, people_nums_confidences, people_nums_pics = self.recognize_people_numbers(tables[0], tables[0])
        
        to_detect = res_info.copy()
        
        l = tr.TransformationInterface.remove_lines(res_info)
        l = tr.TransformationInterface.gain_division(res_info)

        
        to_cropp = l.copy()
        cells_coords = TRI.TableRecognitionInterface.get_cells_coords(to_detect.copy())[1:]
        cells_coords = [sorted(i, key = lambda x: x[0], reverse=False) for i in cells_coords]
        cells_lens = [len(i) for i in cells_coords]
        num_of_rows = len(cells_lens)
        min_num_of_cells_in_row = min(cells_lens)
        max_num_of_cells_in_row = max(cells_lens)
        if num_of_rows != 33 or min_num_of_cells_in_row != 6 or max_num_of_cells_in_row != 6:
            cells_coords = self.big_table_coords
            #     raise CellsNumberException('black_pen_pipeline', num_of_rows, min_num_of_cells_in_row, max_num_of_cells_in_row)
        cropped_cells = [ct.CutterInterface.get_cropped_images(to_cropp, i, ascending=False) for i in cells_coords]
        pics = [object_pic]
        pics.extend(people_nums_pics)
        col1 = [object_num]
        col1.extend(people_nums_vals)
        col2 = ['']
        col2.extend(people_nums_confidences)

        for idy, k in enumerate(cropped_cells):
            digs = []
            conf = []
            vis = ct.CutterInterface.concat_two_imgs(k[0], k[1])
            for i in k[2:]:
                vis = ct.CutterInterface.concat_two_imgs(vis, i)
            pics.append(vis)
            for idx, c in enumerate(k):
                if idx < 5:
                    if idx == 3:
                        digs.append('.')
                    else:

                        rec = self.recognize_digit(c)
                        digs.append(rec[0])
                        conf.append(rec[1])
            col1.append(''.join(digs))
            digs.clear()
            col2.append(''.join(conf))
            conf.clear()
            
        path = f"./imgs_for_excel/{doc_num}"
        Path(path).mkdir(parents=True, exist_ok=True)
        for index, i in enumerate(pics):
            cv2.imwrite(os.path.join(path + '/' , f'{index}.jpg'), i)
            cv2.waitKey(0)
            
        if doc_num == 0:
            df = pd.DataFrame({'index' : [''] * 6 + [_ for _ in range(1, len(col1) - 5)],'Работы' : rows_names,'Кол-во': col1, 'Ув. >80%' : col2})
        else:
            df = pd.DataFrame({'Кол-во': col1, 'Ув. >80%' : col2}) #, index = [_ for _ in range(1, len(col1) + 1)]

        return df

    def blue_pen_pipeline(self, image, doc_num, rows_names):
        blue = tr.TransformationInterface.blue_only(deepcopy(image))
        
        
        res_clear, res_schema = TRI.TableRecognitionInterface.get_subtables2(image, blue)
        
        # for idx, i in enumerate(res_schema):
        #     cv2.imwrite(os.path.join('prototyping/sheets_pdf/detected_cells/' , f'{idx}.jpg'), i)
        #     cv2.waitKey(0)
        
        res_info = res_schema[1][30:, 650:]
        res_info_clear = res_clear[1][30:, 650:]
        
        object_num, object_pic = self.recognize_object_number(res_schema[2])
        
        people_nums_vals, people_nums_confidences, people_nums_pics = self.recognize_people_numbers(res_schema[0], res_clear[0])
        
        to_detect = res_info.copy()
        
        l = tr.TransformationInterface.gain_division(res_info_clear.copy())
        
        to_cropp = l.copy()
        cells_coords = TRI.TableRecognitionInterface.get_cells_coords(to_detect.copy())[1:]
        cells_coords = [sorted(i, key = lambda x: x[0], reverse=False) for i in cells_coords]
        cells_lens = [len(i) for i in cells_coords]

        num_of_rows = len(cells_lens)
        min_num_of_cells_in_row = min(cells_lens)
        max_num_of_cells_in_row = max(cells_lens)
        if num_of_rows != 33 or min_num_of_cells_in_row != 6 or max_num_of_cells_in_row != 6:
            cells_coords = self.big_table_coords
        #     raise CellsNumberException('blue_pen_pipeline', num_of_rows, min_num_of_cells_in_row, max_num_of_cells_in_row)
        cropped_cells = [ct.CutterInterface.get_cropped_images(to_cropp, i, ascending=False) for i in cells_coords]
        pics = [object_pic]
        pics.extend(people_nums_pics)
        
        col1 = [object_num]
        col1.extend(people_nums_vals)
        col2 = ['']
        col2.extend(people_nums_confidences)

        for idy, k in enumerate(cropped_cells):
            digs = []
            conf = []
            vis = ct.CutterInterface.concat_two_imgs(k[0], k[1])
            for i in k[2:]:
                vis = ct.CutterInterface.concat_two_imgs(vis, i)
            pics.append(vis)
            for idx, c in enumerate(k):
                if idx < 5:
                    if idx == 3:
                        digs.append('.')
                    else:
                        rec = self.recognize_digit(c)
                        digs.append(rec[0])
                        conf.append(rec[1])
            col1.append(''.join(digs))
            digs.clear()
            col2.append(''.join(conf))
            conf.clear()
            
        path = f"./imgs_for_excel/{doc_num}"
        Path(path).mkdir(parents=True, exist_ok=True)
        for index, i in enumerate(pics):
            cv2.imwrite(os.path.join(path + '/' , f'{index}.jpg'), i)
            cv2.waitKey(0)
        
        if doc_num == 0:
            null_indexing = [''] * 6 + [_ for _ in range(1, len(col1) - 5)]

            df = pd.DataFrame({'ind' : [''] * 6 + [_ for _ in range(1, len(col1) - 5)],'Работы' : rows_names,'Кол-во': col1, 'Ув. >80%' : col2}, index = [_ for _ in range(1, len(col1) + 1)])
        else:
            df = pd.DataFrame({'Кол-во': col1, 'Ув. >80%' : col2}, index = [_ for _ in range(1, len(col1) + 1)]) #, index = [_ for _ in range(1, len(col1) + 1)]
        return df
    
    def recognition_with_color_context(self, page, rows_names):
        if page.is_blue_pen == False:
            return self.black_pen_pipeline(page.image, page.number, rows_names)
        else:
            return self.blue_pen_pipeline(page.image, page.number, rows_names)
    
    