from openpyxl import Workbook
from openpyxl.utils.dataframe import dataframe_to_rows
from openpyxl.utils import get_column_letter
from openpyxl.drawing.image import Image as Img
import sys
from util import Rec
from util import Doc
from util import CellsNumberException
import util
import os
import logging
import cv2
import json
import shutil
import uvicorn
from fastapi import File, FastAPI, UploadFile
from starlette.responses import FileResponse

def write_to_xslx(array_of_dfs, filename, save_filepath):
    
    wb = Workbook()
    ws = wb.active
    
    skip = 0
    for ind, i in enumerate(array_of_dfs):
        # if ind == 0:
        #     rows = dataframe_to_rows(i, index = False)
        #     for r_idx, row in enumerate(rows, 1):
        #         for c_idx, value in enumerate(row, 1):
        #             ws.cell(row=r_idx, column=c_idx, value=value)
            
        # else:
        rows = dataframe_to_rows(i, index = False)
        add = 0
        for r_idx, row in enumerate(rows, 1):
            if r_idx == 2:
                for c_idx, value in enumerate(row, 1 + skip):
                    ws.cell(row=r_idx, column=c_idx, value='')
                    add = 1
            for c_idx, value in enumerate(row, 1 + skip):
                    ws.cell(row=r_idx + add, column=c_idx, value=value)
        skip += 6
                
        newCol = get_column_letter(ws.max_column+1)
        for i in range(3, array_of_dfs[0].shape[0] + 3):
            img = Img(os.path.join(fr'imgs_for_excel', os.path.join(fr'{ind}', f'{i - 3}.jpg')))
            img.height, img.width = 20, 80
            ws.add_image(img, str(newCol) + str(i))


    for cell in ws['A'] + ws[1]:
        cell.style = 'Pandas'

    wb.save(f"{os.path.join(save_filepath, filename)}.xlsx")
    return f"{os.path.join(save_filepath, filename)}.xlsx"


def for_button(pdf_path):
    if pdf_path == '':
        return
    filename = os.path.split(pdf_path)[-1][:-4]
    rec = Rec.Recognizer()
    doc = Doc.Doc(pdf_path)
    to_excel = []
    # to_excel = [rec.recognition_with_color_context(i, doc.rows_names) for i in doc.pages]
    page_counter = 0
    for idx, i in enumerate(doc.pages):
        try:
            i.number = page_counter
            cur = rec.recognition_with_color_context(i, doc.rows_names)
            to_excel.append(cur)
            page_counter += 1
        except CellsNumberException.CellsNumberException as e:
            logging.error(e)
    # for i in doc.pages:
    #     print(i.is_blue_pen)
    return write_to_xslx(to_excel, f'{filename}', '../storage/recognized')
    # os.system(f"start EXCEL.EXE {filename}.xlsx")


# def set_filepath(app, var):
#     FILEOPENOPTIONS = dict(defaultextension=".pdf", initialdir = "/",
#                        filetypes=[('pdf file', '*.pdf')])
#     app.filename = filedialog.askopenfilename(**FILEOPENOPTIONS)
#     var.set(app.filename)
    
    
# def set_up_and_start_gui():
#     app = Tk()
#
#     app.wm_title("SkyNet Recognition")
#     app.geometry("500x300")
#     for c in range(8): app.rowconfigure(index=c, weight=1)
#     for c in range(3): app.columnconfigure(index=c, weight=1)
#     em = Label(text="Путь к файлу:")
#     em.grid(row=0, column=1, rowspan=2, ipadx=6, ipady=6, padx=4, pady=4, sticky="nsew")
#     app.filename = ''
#     var = StringVar()
#     textbox = Entry(textvariable=var)
#     textbox.focus_set()
#     textbox.grid(row=2, column=0, columnspan=3, ipadx=2, ipady=2, padx=2, pady=2, sticky="nsew")
#
#     fil = Button(app, text="Найти файл", command=lambda: set_filepath(app, var))
#     fil.grid(row=3, column=1,rowspan=2, ipadx=6, ipady=6, padx=4, pady=4, sticky="nsew")
#     sign_in = Button(app, text="Распознать", command= lambda: for_button(app.filename, ''))
#     sign_in.grid(row=5, column=1,ipadx=6,rowspan=2, ipady=6, padx=4, pady=4, sticky="nsew")
#
#     app.mainloop()

def image_rects(img, coords):
    for i in coords:
        (x, y, w, h) = i
        cv2.rectangle(img, (x, y), (x + w, y + h), (36, 255, 12), 3)
    return img


app = FastAPI()

@app.get("/")
def read_root():
    return {"message": "Welcome from the API"}
@app.post("/excel")
def get_image(file: UploadFile = File(...)):
    # try:
    headers = {
        # By adding this, browsers can download this file.
        'Content-Disposition': f'attachment; filename=book.xlsx',
        # Needed by our client readers, for CORS (cross origin resource sharing).
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "*",
        "Access-Control_Allow-Methods": "POST, GET, OPTIONS",
    }
    store_dir = '../storage/input'
    out_dir = '../storage/recognized'
    full_filepath = os.path.join(store_dir, file.filename)
    with open(full_filepath, 'wb') as f:
        shutil.copyfileobj(file.file, f)
    res_path = for_button(full_filepath)
    return {'filepath': os.path.join(out_dir, file.filename.split('.')[0] + '.xlsx')}
    # except Exception:
    #     return {"message": "There was an error uploading the file"}
    # finally:
    #     file.file.close()






if __name__ == '__main__':
    uvicorn.run("main:app", host="0.0.0.0", port=8080)
    # for_button(r'C:\Users\Murk\Downloads\real_sample2.pdf', '')